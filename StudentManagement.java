public class StudentManagement {
    private Student[] students = new Student[100];
    public static boolean sameGroup(Student s1, Student s2) {
        return (s1.getGroup().equals(s2.getGroup()));
    }
    private int currentStudents = 0;
    public void addStudent(Student newStudent){
        students[currentStudents] = newStudent;
        currentStudents ++;
    }
    public String studentsByGroup(){
        Student firstStudent = students[0];
        boolean[] check = new boolean[currentStudents];
        for (int j = 0; j < currentStudents; j++){
            check[j] = false;
        }
        String result = "";
        for (int k = 0; k < currentStudents; k++){
            if (!check[k]){
                firstStudent = students[k];
                result = result + firstStudent.getGroup() + "\n";
                for (int n = k; n < currentStudents; n++){
                    if (sameGroup(students[n], firstStudent) && !check[n]){
                        check[n] = true;
                        result += students[n].getInfo() + "\n";
                    }
                }
            }

        }
        return result;
    }
    public void removeStudent(String id){
        for (int n = 0; n < currentStudents; n++){
            if (students[n].getId().equals(id)){
                for (int j = n; j < currentStudents; j++){
                    students[j] = students[j + 1];
                }
                n--;
                currentStudents--;
            }
        }
    }
    public static void main(String[] args) {

    }
}