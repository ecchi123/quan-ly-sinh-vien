public class Student {
    private String name;
    private String id;
    private String group;
    private String email;

    public void setName(String n){
        name = n;
    }
    public void setId(String n){
        id = n;
    }
    public void setGroup(String n){
        group = n;
    }
    public void setEmail(String n){
        email = n;
    }
    public String getName(){
        return name;
    }
    public String getId(){
        return id;
    }
    public String getGroup(){
        return group;
    }
    public String getEmail(){
        return email;
    }
    public Student(){
        name = "Student";
        id = "000";
        group = "K62CB";
        email = "uet@vnu.edu.vn";
    }
    public Student(String name, String id, String email){
        this.name = name;
        this.id = id;
        this.email = email;
        group = "K62CB";
    }
    public Student(Student s){
        this.name = s.getName();
        this.id = s.getId();
        this.group = s.getGroup();
        this.email = s.getEmail();
    }
    public String getInfo(){
        String result = name + " - " + id + " - " + group + " - " + email;
        return result;
    }
}
